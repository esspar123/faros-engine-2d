﻿namespace Faros_Engine_2D
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.new_projectButton = new System.Windows.Forms.ToolStripMenuItem();
            this.open_project_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.mainsettings_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.exit_Button = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(995, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.new_projectButton,
            this.toolStripSeparator2,
            this.open_project_Button,
            this.toolStripSeparator3,
            this.mainsettings_Button,
            this.toolStripSeparator4,
            this.exit_Button});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(37, 20);
            this.toolStripComboBox1.Text = "File";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // new_projectButton
            // 
            this.new_projectButton.Name = "new_projectButton";
            this.new_projectButton.Size = new System.Drawing.Size(180, 22);
            this.new_projectButton.Text = "New Project";
            this.new_projectButton.Click += new System.EventHandler(this.new_projectButton_Click);
            // 
            // open_project_Button
            // 
            this.open_project_Button.Name = "open_project_Button";
            this.open_project_Button.Size = new System.Drawing.Size(180, 22);
            this.open_project_Button.Text = "Open Project";
            this.open_project_Button.Click += new System.EventHandler(this.open_project_Button_Click);
            // 
            // mainsettings_Button
            // 
            this.mainsettings_Button.Name = "mainsettings_Button";
            this.mainsettings_Button.Size = new System.Drawing.Size(180, 22);
            this.mainsettings_Button.Text = "Settings";
            this.mainsettings_Button.Click += new System.EventHandler(this.mainsettings_Button_Click);
            // 
            // exit_Button
            // 
            this.exit_Button.Name = "exit_Button";
            this.exit_Button.Size = new System.Drawing.Size(180, 22);
            this.exit_Button.Text = "Quit";
            this.exit_Button.Click += new System.EventHandler(this.exit_Button_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(177, 6);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(177, 6);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(995, 532);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Faros Engine 2D";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripComboBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem new_projectButton;
        private System.Windows.Forms.ToolStripMenuItem open_project_Button;
        private System.Windows.Forms.ToolStripMenuItem mainsettings_Button;
        private System.Windows.Forms.ToolStripMenuItem exit_Button;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    }
}

